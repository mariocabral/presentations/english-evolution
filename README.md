

## Access

public access: http://mlc-code.gitlab.io/english-evolution/#/cover



## Speak

### Presentation:

Slide 1:  Hello, I talk about the English language. Their evolution that has over the time, from their origins to the current state of this language. 

Slide 2: This presentation has the next  topics:
- In the First place, I will be talking about the origins of the English and the relationship with other languages.
- In the second place, I will be talking about the old English, when started and some events of this period.
- In the three place, I will be showing a timeline that has the principal events of the middle and modern English.  
- After that, I will be talking about the change of the pronunciation over the time.
- The next slide I will be showing some examples of the changes between the English of the different periods.
- Finally, I will show the status of the English at this time.

Slide 3:   The West Germanic languages constitute the largest of the three branches of the Germanic family of languages:
- Erminonic:  Irminonic or Elbe Germanic is a conventional term grouping early West Germanic dialects ancestral to High German,[3] which would include modern Standard German.
- They categorized them as one of the nations of Germanic tribes descended from one of the sons of Mannus, a Germanic ancestor
- The Ingaevones, or North Sea Germanic peoples.


Slide 4: In this slide, you can see the locations of the places that spoke their languages. In the north, are the tribes that speak the language that becomes in the English language.

Slide 5: Now, I speak of the origins of the English, the Old English.

Slide 6: This map helps us to understand where Briton's defenders, then invaders, originated after Rome's legions left Britannia undefended. At first, the Britons were able to pay their foreign defenders. But as more and more Anglo-Saxons descended on Britain, compensating them became increasingly difficult. Soon the immigrants "took" what was not theirs. 
Celtic tribes, who had caused Britons to seek help in the first place, continued to inhabit the northern part of the territory. Fierce as they were, they remained independent during this time frame.
  Anglo-Saxon England "settled down" into five major Kingdoms: Northumbria, East Anglia, Wessex, Kent, and Mercia.  The whole idea of statehood was better developed in Wessex and Mercia.  Northumbria was a kind of expansive borderland, and East Anglia and Kent were the Anglo-Saxon heartlands, where the polities were weaker.

Slide 7: 
- This period starts at 500 years and ends in 1100 years. 
- Some events that ended this period was the wars, starting from the 8th century with the Viking and the Normand Conquest of England.

Slide 8:  This figure shows some words in Old English.

Slide 9: (timeline)  

Slide 10: The Great Vowel Shift was a major series of changes in the pronunciation of the English language that took place, beginning in southern England, primarily between 1350 and the 1600s and 1700s, today influencing effectively all dialects of English. English spelling was first becoming standardized in the 15th and 16th centuries, and the Great Vowel Shift is responsible for the fact that English spellings now often strongly deviate in their representation of English pronunciations.

Slide 11: This timeline shows the main vowel changes that occurred between late Middle English in the year 1400 and Received Pronunciation in the mid-20th century by using representative words. The Great Vowel Shift occurred in the lower half of the table, between 1400 and 1600. 

Slide 12: This is a simplified picture of the changes that happened between late Middle English and today's English. Pronunciations in 1400, 1500, 1600, and 1900 are shown.[5] To hear recordings of the sounds, click the phonetic symbols.


